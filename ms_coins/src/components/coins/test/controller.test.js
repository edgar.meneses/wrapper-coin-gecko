const { assert, expect } = require('chai');
const {
  createSandbox,
  stub,
  spy,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../../commons/errors/wrapperError');

let sandbox;
let req;
let res;
let next;
let controllerApp;
let findCoins;
let saveCoin;
let findQuotation;

describe('Conins controller', () => {
  beforeEach(() => {
    sandbox = createSandbox();

    res = {
      status: stub(),
      json: stub(),
    };
    res.status.returns(res);

    next = spy();

    controllerApp = rewire('../controller.js');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Find all list of coins', () => {
    beforeEach(() => {
      findCoins = controllerApp.__get__('findCoins');
    });

    it('Should return list of coins', async () => {
      const coins = [{ name: 'Bitcoin' }, { name: 'Ethereum' }, { name: 'VeChain' }];
      req = {
        query: {
          pageNumber: 2,
          pageSize: 4,
          order: 'ms_order',
        },
      };

      controllerApp.__set__('findAllCoins', () => coins);

      await findCoins(req, res, next);

      expect(res.json.args[0][0].data.coinList).eql(coins);
      assert.deepEqual(res.json.args[0][0].links, { currentPage: 2, nextPage: 3, pageSize: 4 });
    });

    it('Should return list of coins when pageNumber is not in query', async () => {
      const coins = [{ name: 'Bitcoin' }, { name: 'Ethereum' }, { name: 'VeChain' }];
      req = {
        query: {
          pageSize: 4,
          order: 'ms_order',
        },
      };

      controllerApp.__set__('findAllCoins', () => coins);

      await findCoins(req, res, next);

      expect(res.json.args[0][0].data.coinList).eql(coins);
      assert.deepEqual(res.json.args[0][0].links, { currentPage: 1, nextPage: 2, pageSize: 4 });
    });

    it('Should return list of coins when pageSize is not in query', async () => {
      const coins = [{ name: 'Bitcoin' }, { name: 'Ethereum' }, { name: 'VeChain' }];
      req = {
        query: {
          pageNumber: 2,
          order: 'ms_order',
        },
      };

      controllerApp.__set__('findAllCoins', () => coins);

      await findCoins(req, res, next);

      expect(res.json.args[0][0].data.coinList).eql(coins);
      assert.deepEqual(res.json.args[0][0].links, { currentPage: 2, nextPage: 3, pageSize: 25 });
    });

    it('Should return list of coins when order is not in query', async () => {
      const coins = [{ name: 'Bitcoin' }, { name: 'Ethereum' }, { name: 'VeChain' }];
      req = {
        query: {
          pageNumber: 3,
          pageSize: 4,
        },
      };

      controllerApp.__set__('findAllCoins', () => coins);

      await findCoins(req, res, next);

      expect(res.json.args[0][0].data.coinList).eql(coins);
      assert.deepEqual(res.json.args[0][0].links, { currentPage: 3, nextPage: 4, pageSize: 4 });
    });

    it('Should return error when fin coins fail', async () => {
      const errorData = ['WC09', 500, 'Error al consultar la lista de monedas'];
      req = {
        query: {
          pageNumber: 2,
          pageSize: 4,
          order: 'ms_order',
        },
      };

      controllerApp.__set__('findAllCoins', () => Promise.reject(new Error()));
      controllerApp.__set__('getError', () => new WrapperError(...errorData));

      await findCoins(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });
  });

  describe('Save coin', () => {
    beforeEach(() => {
      saveCoin = controllerApp.__get__('saveCoin');
    });

    it('Should save coin when coin exists in coingecko', async () => {
      const coin = {
        id: 'ZW1lbmVzZXMjdXNkJmJpdGNvaW4=',
        coinId: 'bitcoin',
        currency: 'usd',
        username: 'emeneses',
        isFavorite: false,
      };
      req = {
        user: { id: 'ZW1lbmVzZXM=', currency: 'usd' },
        body: {
          coinId: 'bitcoin',
          isFavorite: false,
        },
      };

      controllerApp.__set__('saveCoinService', () => coin);
      controllerApp.__set__('findCoinById', () => coin);

      await saveCoin(req, res, next);

      assert.deepEqual(res.json.args[0][0].data.coinInfo, coin);
    });

    it('Should save coin when coin exists in coingecko and it is a favorite coin', async () => {
      const coin = {
        id: 'ZW1lbmVzZXMjdXNkJmJpdGNvaW4=',
        coinId: 'bitcoin',
        currency: 'usd',
        username: 'emeneses',
        isFavorite: true,
      };
      req = {
        user: { id: 'ZW1lbmVzZXMT=', currency: 'usd' },
        body: {
          coinId: 'bitcoin',
          isFavorite: false,
        },
      };

      controllerApp.__set__('saveCoinService', () => coin);
      controllerApp.__set__('findCoinById', () => coin);

      await saveCoin(req, res, next);

      assert.deepEqual(res.json.args[0][0].data.coinInfo, coin);
    });

    it('Should save coin when coin exists in coingecko and isFavorite is not in query', async () => {
      const coin = {
        id: 'ZW1lbmVzZXMjdXNkJmJpdGNvaW4=',
        coinId: 'bitcoin',
        currency: 'usd',
        username: 'emeneses',
        isFavorite: false,
      };
      req = {
        user: { id: 'ZW1lbmVzZXMT=', currency: 'usd' },
        body: {
          coinId: 'bitcoin',
        },
      };

      controllerApp.__set__('saveCoinService', () => coin);
      controllerApp.__set__('findCoinById', () => coin);

      await saveCoin(req, res, next);

      assert.deepEqual(res.json.args[0][0].data.coinInfo, coin);
    });

    it('Should return error when coin did not exist in coingecko', async () => {
      const errorData = ['WC091', 404, 'Coint not found'];
      req = {
        user: { id: 'ZW1lbmVzZXMTM=', currency: 'usd' },
        body: {
          coinId: 'bitcoin',
        },
      };

      controllerApp.__set__('findCoinById', () => Promise.reject(new Error()));
      controllerApp.__set__('getError', () => new WrapperError(...errorData));

      await saveCoin(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });

    it('Should return error when save coin fail', async () => {
      const errorData = ['WC092', 500, 'Error al guardar la moneda'];
      req = {
        user: { id: 'ZW1lbmVzZXMTMT=', currency: 'usd' },
        body: {
          coinId: 'bitcoin',
        },
      };

      controllerApp.__set__('findCoinById', () => ({ coinId: 'bitcoin' }));
      controllerApp.__set__('saveCoinService', () => Promise.reject(new Error()));
      controllerApp.__set__('getError', () => new WrapperError(...errorData));

      await saveCoin(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });
  });

  describe('Find quotation list', () => {
    beforeEach(() => {
      findQuotation = controllerApp.__get__('findQuotation');
    });

    it('Should return quotation list with order is asc and limit equal 25', async () => {
      const quotationList = [
        { id: '1', coinId: 'bitcoin', price: 10 },
        { id: '2', coinId: 'bitcoin1', price: 20 },
        { id: '3', coinId: 'bitcoin2', price: 30 },
      ];
      req = {
        user: {
          id: 'ZW1lbmVzZXM=',
          currency: 'eur',
        },
        query: {
          order: 'asc',
          limit: 25,
        },
      };

      controllerApp.__set__('findCoinsByQuotation', () => quotationList);

      await findQuotation(req, res, next);

      expect(res.json.args[0][0].data.quotationList, quotationList);
      assert.deepEqual(res.json.args[0][0].links, { limit: 25 });
    });

    it('Should return quotation list with order is desc and limit equal 25', async () => {
      const quotationList = [
        { id: '1', coinId: 'bitcoin', price: 30 },
        { id: '2', coinId: 'bitcoin1', price: 20 },
        { id: '3', coinId: 'bitcoin2', price: 10 },
      ];
      req = {
        user: {
          id: 'ZW1lbmVzZXMM=',
          currency: 'eur',
        },
        query: {
          order: 'desc',
          limit: 25,
        },
      };

      controllerApp.__set__('findCoinsByQuotation', () => quotationList);

      await findQuotation(req, res, next);

      expect(res.json.args[0][0].data.quotationList, quotationList);
      assert.deepEqual(res.json.args[0][0].links, { limit: 25 });
    });

    it('Should return quotation list wihen limit is not in query', async () => {
      const quotationList = [
        { id: '1', coinId: 'bitcoin', price: 10 },
        { id: '2', coinId: 'bitcoin1', price: 20 },
        { id: '3', coinId: 'bitcoin2', price: 30 },
      ];
      req = {
        user: {
          id: 'ZW1lbmVzZXM=',
          currency: 'eur',
        },
        query: {
          order: 'asc',
        },
      };

      controllerApp.__set__('findCoinsByQuotation', () => quotationList);

      await findQuotation(req, res, next);

      expect(res.json.args[0][0].data.quotationList, quotationList);
      assert.deepEqual(res.json.args[0][0].links, { limit: 25 });
    });

    it('Should return error when find quotation fail', async () => {
      const errorData = ['WC097', 500, 'No es ṕosible consultar la información'];
      req = {
        user: {
          id: 'ZW1lbmVzZXM=',
          currency: 'eur',
        },
        query: {
          order: 'asc',
          limit: 25,
        },
      };

      controllerApp.__set__('findCoinsByQuotation', () => Promise.reject(new Error()));
      controllerApp.__set__('getError', () => new WrapperError(...errorData));

      await findQuotation(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });
  });
});
