const { query } = require('express-validator');

const CoinList = [
  query('order')
    .optional()
    .matches(/^[a-zA-Z_]*$/, 'g')
    .withMessage('El campo order no contiene un valor valido')
    .isLength({ min: 0, max: 50 })
    .withMessage('El campo order no contiene una longitud valida'),
  query('pageNumber')
    .optional()
    .isNumeric()
    .withMessage('El campo pageNumber no contiene un valor valido')
    .isInt({ min: 1 })
    .withMessage('El campo pageNumber no contiene un valor valido'),
  query('pageSize')
    .optional()
    .isNumeric()
    .withMessage('El campo pageSize no contiene un valor valido')
    .isInt({ min: 1, max: 250 })
    .withMessage('El campo pageSize no contiene un valor valido'),
];

module.exports = CoinList;
