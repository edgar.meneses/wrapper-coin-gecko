const { query } = require('express-validator');

const Quotation = [
  query('order')
    .optional()
    .isAlpha()
    .withMessage('el campo order no contiene una valor valida')
    .isLength(({ min: 3, max: 5 }))
    .withMessage('el campo order no contiene una logintud valida'),
  query('limit')
    .optional()
    .isNumeric()
    .withMessage('El campo limit no contiene un valor valido')
    .isInt({ min: 1, max: 25 })
    .withMessage('El campo limit no contiene un valor valido'),

];

module.exports = Quotation;
