const { body } = require('express-validator');

const Coin = [
  body('coinId')
    .isAlpha()
    .withMessage('El campo coinId no contiene un valor valido')
    .notEmpty()
    .withMessage('El campo coinId es requerido')
    .isLength({ min: 3, max: 250 })
    .withMessage('el campo CoinId no tiene una longitud valida'),
  body('isFavorite')
    .optional()
    .isBoolean()
    .withMessage('El campo isFavorite no contiene un valor valido'),
];

module.exports = Coin;
