const { decode } = require('js-base64');
const {
  findAllCoins,
  saveCoin: saveCoinService,
  findCoinById,
  findCoinsByQuotation,
} = require('./service');
const { getError } = require('../../commons/errors/handlerError');

const findCoins = async (req, res, next) => {
  try {
    const {
      pageNumber = 1,
      pageSize = 25,
      order = 'market_cap_desc',
    } = req.query;
    const params = {
      vs_currency: 'USD',
      order,
      page: pageNumber,
      per_page: pageSize,
    };

    const nextPage = parseInt(pageNumber, 10) + 1;

    const coins = await findAllCoins(params);

    res.status(200).json({
      data: {
        coinList: coins,
      },
      links: {
        currentPage: parseInt(pageNumber, 10),
        nextPage,
        pageSize: parseInt(pageSize, 10),
      },
    });
  } catch (e) {
    next(getError(e, 'coinList', { method: 'controller - findCoins' }));
  }
};

const saveCoin = async (req, res, next) => {
  try {
    const { id: userId, currency } = req.user;
    const username = decode(userId);
    const id = `${username}#${currency}`;
    const { coinId, isFavorite = false } = req.body;

    const coin = await findCoinById(coinId, currency);

    const newCoin = await saveCoinService({
      ...coin,
      id,
      coinId,
      currency,
      username,
      isFavorite,
    });

    res.status(200)
      .json({ data: { coinInfo: newCoin } });
  } catch (e) {
    next(getError(e, 'saveCoin', { method: 'controller - saveCoin ' }));
  }
};

const findQuotation = async (req, res, next) => {
  try {
    const { id, currency } = req.user;
    const { order, limit = 25 } = req.query;
    const hashKey = `${decode(id)}#${currency}`;
    const data = await findCoinsByQuotation(hashKey, order.toLowerCase(), limit);
    res.status(200).json({
      data: { quotationList: data },
      links: { limit: parseInt(limit, 10) },
    });
  } catch (e) {
    next(getError(e, 'findQuotation', { method: 'Controller - findQuotation' }));
  }
};

module.exports = {
  findCoins,
  saveCoin,
  findQuotation,
};
