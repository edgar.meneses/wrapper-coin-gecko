const { encode } = require('js-base64');
const { httpGetRequest } = require('../../commons/request');
const { getError } = require('../../commons/errors/handlerError');
const config = require('../../startup/config');
const { saveItem, query } = require('../../commons/aws/dynamodb');

const handlerCoinList = (coin) => ({
  symbol: coin.symbol,
  name: coin.name,
  image: coin.image,
  price: coin.current_price,
  lastUpdate: coin.last_updated,
});

const findAllCoins = async (params) => {
  try {
    const coins = await httpGetRequest(`${config.coingeckoURL}/coins/markets`,
      'coinList', { ...params, sparkline: false });

    return coins.map(handlerCoinList);
  } catch (e) {
    throw getError(e, 'coinList', { method: 'service - findCoins' });
  }
};

const handlerCoin = (coin, order) => ({
  ...coin,
  image: coin.image.large,
  orderBy: (order === 'desc') ? `${coin.currency}_desc` : `${coin.currency}_asc`,
  coinId: undefined,
  username: undefined,
  currency: undefined,
  id: encode(`${coin.id}&${coin.coinId}`),
});

const findCoinById = async (cointId, currency) => {
  try {
    const coin = await httpGetRequest(`${config.coingeckoURL}/coins/${cointId}`, 'findCoinById');
    const marketPrice = coin.market_data.current_price;
    return {
      symbol: coin.symbol,
      name: coin.name,
      image: coin.image,
      price: marketPrice[currency],
      marketPrice: {
        usd: marketPrice.usd,
        eur: marketPrice.eur,
        ars: marketPrice.ars,
      },
      lastUpdate: coin.last_updated,
    };
  } catch (e) {
    throw getError(e, 'findCoinById', { method: 'service - findCoinById' });
  }
};

const saveCoin = async (coin) => {
  try {
    await saveItem(coin, config.coinsTable.name);
    return handlerCoin(coin);
  } catch (e) {
    throw getError(e, 'saveCoin', { method: 'service - saveCoin' });
  }
};

const findCoinsByQuotation = async (hashKey, order, limit) => {
  try {
    const params = {
      TableName: config.coinsTable.name,
      IndexName: config.coinsTable.priceIndexName,
      KeyConditionExpression: 'id = :id',
      ExpressionAttributeValues: {
        ':id': hashKey,
      },
      Limit: limit,
    };

    const quotationList = await query(params, order);
    return quotationList.map(handlerCoin);
  } catch (e) {
    throw getError(e, 'findQuotation', { method: 'Service - findQuotation' });
  }
};

module.exports = {
  findCoinById,
  findAllCoins,
  saveCoin,
  findCoinsByQuotation,
};
