const express = require('express');
const sanitization = require('../../middleware/sanitization');
const CoinSchema = require('./schema/Coin');
const Quotation = require('./schema/Quotation');
const CoinList = require('./schema/CoinList');

const router = express.Router();
const { findCoins, saveCoin, findQuotation } = require('./controller');

router.get('/', CoinList, sanitization, findCoins);
router.post('/', CoinSchema, sanitization, saveCoin);
router.get('/quotation', Quotation, sanitization, findQuotation);

module.exports = router;
