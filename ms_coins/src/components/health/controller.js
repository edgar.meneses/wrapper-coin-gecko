const healthCheck = (req, res) => {
  const data = { message: 'Server running success' };

  res.status(200).json({ data });
};

module.exports = {
  healthCheck,
};
