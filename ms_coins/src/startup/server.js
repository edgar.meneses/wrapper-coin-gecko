const express = require('express');
const routes = require('./routes');
const { info } = require('../commons/logger');

const app = express();

const startServer = (_config) => {
  routes(app);
  return app.listen(_config.port || '9091', () => {
    info(`Running at port ${_config.port} on environment ${_config.env}`);
  });
};

module.exports = startServer;
