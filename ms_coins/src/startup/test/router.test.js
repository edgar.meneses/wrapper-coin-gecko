const { assert } = require('chai');
const {
  spy,
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');

let sandbox;
let helmet;
let configRoutes;
let app;
let contentSecurityPolicy;
let referrerPolicy;
let noCache;
let bodyParser;
let morgan;

describe('Route definitions suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });
  describe('Configure Routes', () => {
    beforeEach(() => {
      helmet = stub();
      contentSecurityPolicy = spy();
      referrerPolicy = spy();
      noCache = spy();
      bodyParser = {
        json: spy(),
        urlencoded: spy(),
      };
      morgan = spy();

      const appRoutes = rewire('../routes');
      appRoutes.__set__('helmet', helmet);
      appRoutes.__set__('helmet.contentSecurityPolicy', contentSecurityPolicy);
      appRoutes.__set__('helmet.referrerPolicy', referrerPolicy);
      appRoutes.__set__('noCache', noCache);
      appRoutes.__set__('bodyParser', bodyParser);
      appRoutes.__set__('healthCheck', spy());
      appRoutes.__set__('morgan', morgan);
      appRoutes.__set__('userRoutes', spy());

      configRoutes = appRoutes.__get__('configRoutes');

      app = {
        use: spy(),
      };
    });

    it('app.use should be called when configRoutes is executed', async () => {
      await configRoutes(app);

      assert.isTrue(app.use.called);
    });

    it('helmet should be called when configureRoutes is executed', () => {
      configRoutes(app);

      assert.isTrue(helmet.called);
    });

    it('helmet.contentSecurityPolicy should be called when configureRoutes is executed', () => {
      configRoutes(app);

      assert.isTrue(contentSecurityPolicy.called);
    });

    it('helmet.referrerPolicy should be called when configureRoutes is executed', () => {
      configRoutes(app);

      assert.isTrue(referrerPolicy.called);
    });

    it('noCache should be called when configureRoutes is executed', () => {
      configRoutes(app);

      assert.isTrue(noCache.called);
    });

    it('bodyParser.json should be called when configureRoutes is executed', () => {
      configRoutes(app);

      assert.isTrue(bodyParser.json.called);
    });

    it('bodyParser.urlencoded should be called when configureRoutes is executed', () => {
      configRoutes(app);

      assert.isTrue(bodyParser.urlencoded.called);
    });

    it('bodyParser.urlencoded should be called when configureRoutes is executed', () => {
      configRoutes(app);

      assert.isTrue(morgan.called);
    });
  });
});
