const { assert } = require('chai');
const {
  spy,
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../commons/errors/wrapperError');

let config;
let configApp;
let sandbox;
let getConfig;

describe('Server start suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
    configApp = rewire('../config.js');

    const getErrorMock = stub();
    getErrorMock.returns(new WrapperError(...['WCG', 404, 'No se encontró el archivo']));

    configApp.__set__('getError', getErrorMock);
    configApp.__set__('error', spy());
    config = configApp.__get__('config');
  });
  afterEach(() => {
    sandbox.restore();
  });

  describe('get config data from object config', () => {

    it('Should return port when config is called', () => {
      assert.equal(config.port, 9093);
    });

    it('Should return environment when config is called', () => {
      assert.equal(config.env, 'local');
    });
  });

  describe('get config environment from function getConfig', () => {
    beforeEach(() => {
      getConfig = configApp.__get__('getConfig');
    });

    it('should return true when convict loadFile, validate and get is called', () => {
      const convictMock = stub();
      const loadFile = spy();
      const get = spy();
      const validate = stub();
      convictMock.returns({
        loadFile,
        get,
        validate,
      });
      configApp.__set__('convict', convictMock);

      getConfig();

      assert.isTrue(loadFile.called);
      assert.isTrue(validate.called);
      assert.equal(get.args[0][0], 'env');
    });

    it('should return port value when function getConfig is called and process.env.ENVIRONMENT is undefined', () => {
      const port = getConfig().get('port');

      assert.equal(port, '9093');
    });

    it('should return env value when function getConfig is called and process.env.ENVIRONMENT is undefined', () => {
      const env = getConfig().get('env');

      assert.equal(env, 'local');
    });

    it('should return env value when function getConfig is called and process.env.ENVIRONMENT is dev', () => {
      process.env.ENVIRONMENT = 'dev';

      const env = getConfig().get('env');

      assert.equal(env, 'dev');
    });

    it('should return env value when function getConfig is called and process.env.ENVIRONMENT is qa', () => {
      process.env.ENVIRONMENT = 'qa';

      const env = getConfig().get('env');

      assert.equal(env, 'qa');
    });

    it('should return env value when function getConfig is called and process.env.ENVIRONMENT is pdn', () => {
      process.env.ENVIRONMENT = 'pdn';

      const env = getConfig().get('env');

      assert.equal(env, 'pdn');
    });

    it('should return error when function getConfig is called and file env.json do not exits', () => {
      process.env.ENVIRONMENT = 'my-env';

      try {
        getConfig().get('env');
      } catch (e) {
        assert.equal(e.message, 'No se encontró el archivo');
        assert.equal(e.code, 'WCG');
        assert.equal(e.status, 404);
      }
    });
  });
});
