require('dotenv').config();
const convict = require('convict');
const schema = require('./config/schema');
const { getError } = require('../commons/errors/handlerError');
const { error } = require('../commons/logger');

const getConfig = () => {
  try {
    const config = convict(schema);
    config.loadFile(`./src/startup/config/environment/${config.get('env')}.json`);
    config.validate({ strict: true });
    return config;
  } catch (e) {
    const env = process.env.ENVIRONMENT;
    const wrapperError = getError(e, 'loadConvictFile',
      { method: 'getConfig', env });
    error(wrapperError);
    throw wrapperError;
  }
};

const convictConfig = getConfig();
const config = {
  env: convictConfig.get('env'),
  port: convictConfig.get('port'),
  coinsTable: convictConfig.get('coinsTable'),
  aws: convictConfig.get('aws'),
  roundSalt: 10,
  oauthURL: convictConfig.get('oauthRequestURL'),
  coingeckoURL: convictConfig.get('coingeckoURL'),
};

module.exports = config;
