const AWS = require('..');

const dynamoDB = new AWS.DynamoDB.DocumentClient();
const { getError } = require('../../errors/handlerError');

const saveItem = async (item, tableName) => {
  const params = {
    TableName: tableName,
    Item: {
      ...item,
    },
    ReturnValues: 'ALL_OLD',
    ReturnConsumedCapacity: 'INDEXES',
    ReturnItemCollectionMetrics: 'SIZE',
  };

  return new Promise((resolve, reject) => {
    dynamoDB.put(params, (err, data) => {
      if (err) {
        reject(getError(err, 'saveUser', { method: 'database' }));
      } else {
        resolve(data);
      }
    });
  });
};

const getItem = (key, tableName) => {
  const params = {
    Key: key,
    TableName: tableName,
    ConsistentRead: true,
  };

  return new Promise((resolve, reject) => {
    dynamoDB.get(params, (err, data) => {
      if (err) {
        reject(getError(err, 'findUser', { method: 'database' }));
      } else {
        resolve(data.Item);
      }
    });
  });
};

const query = (params, order) => {
  const configParams = {
    ...params,
    ConsistentRead: true,
    ReturnConsumedCapacity: 'NONE',
    ScanIndexForward: (order !== 'desc'),
  };

  return new Promise((resolve, reject) => {
    dynamoDB.query(configParams, (err, data) => {
      if (err) {
        reject(getError(err, 'findQuotation', { method: 'database' }));
      } else {
        resolve(data.Items);
      }
    });
  });
};

module.exports = {
  saveItem,
  getItem,
  query,
};
