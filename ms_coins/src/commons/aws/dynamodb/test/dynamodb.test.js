const { assert, expect } = require('chai');
const {
  createSandbox,
  stub,
} = require('sinon');
const rewire = require('rewire');

let sandbox;
let dynamodbAPP;
let saveItem;
let getItem;
let dynamoDBSpy;
let item;
let query;

describe('Dynamodb options', () => {
  beforeEach(() => {
    item = {
      username: 'emeneses',
      firstName: 'Edgar',
      lastName: 'Meneses',
    };

    sandbox = createSandbox();
    dynamodbAPP = rewire('../index.js');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Dynamodb put items', () => {
    beforeEach(() => {
      saveItem = dynamodbAPP.__get__('saveItem');
    });

    it('Should return user save when dynamodb.put run success', async () => {
      dynamoDBSpy = {
        put: (params, callback) => {
          callback(null, { ...item });
        },
      };

      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);

      const newUser = await saveItem(item, 'users');

      assert.deepEqual(newUser, item);
    });

    it('Should return error when dynamodb.put fail', async () => {
      dynamoDBSpy = {
        put: (prams, callback) => {
          callback(new Error(''));
        },
      };
      const errorSpy = stub();
      errorSpy.returns(new Error('Error al guardar la información'));

      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);
      dynamodbAPP.__set__('getError', errorSpy);
      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);
      dynamodbAPP.__set__('getError', errorSpy);

      try {
        await saveItem(item, 'users');
      } catch (e) {
        assert.equal(e.message, 'Error al guardar la información');
      }
    });
  });

  describe('Dynamodb get item', () => {
    beforeEach(() => {
      getItem = dynamodbAPP.__get__('getItem');
    });

    it('Should return item when dynamodb.get run success', async () => {
      dynamoDBSpy = {
        get: (prams, callback) => {
          callback(null, { Item: { ...item } });
        },
      };
      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);

      const itemData = await getItem({ username: 'emeneses' }, 'user');

      assert.deepEqual(itemData, item);
    });

    it('Should return error when dynamodb.get fail', async () => {
      dynamoDBSpy = {
        get: (prams, callback) => {
          callback(new Error());
        },
      };
      const errorSpy = stub();
      errorSpy.returns(new Error('Error al consultar el item'));

      dynamodbAPP.__set__('getError', errorSpy);
      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);

      try {
        await getItem({ username: 'emeneses' }, 'user');
      } catch (e) {
        assert.equal(e.message, 'Error al consultar el item');
      }
    });
  });

  describe('Filter data using query', () => {
    beforeEach(() => {
      query = dynamodbAPP.__get__('query');
    });

    it('Should return data when query is success', async () => {
      const params = { Table: 'my-table', index: 'my-index' };
      const dataList = [{ id: 1, price: 10 }, { id: 2, price: 20 }];
      dynamoDBSpy = {
        query: (prams, callback) => {
          callback(null, { Items: dataList });
        },
      };

      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);

      const list = await query(params);

      expect(list).to.eql(dataList);
    });

    it('Should return data order by price asc query is success', async () => {
      const params = { Table: 'my-table', index: 'my-index' };
      const dataList = [{ id: 1, price: 10 }, { id: 2, price: 20 }];
      dynamoDBSpy = {
        query: (prams, callback) => {
          callback(null, { Items: dataList });
        },
      };

      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);

      const list = await query(params, 'asc');

      expect(list).to.eql(dataList);
    });

    it('Should return data order by price desc query is success', async () => {
      const params = { Table: 'my-table', index: 'my-index' };
      const dataList = [{ id: 2, price: 20 }, { id: 1, price: 10 }];
      dynamoDBSpy = {
        query: (prams, callback) => {
          callback(null, { Items: dataList });
        },
      };

      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);

      const list = await query(params, 'desc');

      expect(list).to.eql(dataList);
    });

    it('Should return error when query fail', async () => {
      const params = { Table: 'my-table', index: 'my-index' };
      dynamoDBSpy = {
        query: (prams, callback) => {
          callback(new Error());
        },
      };

      const errorSpy = stub();
      errorSpy.returns(new Error('Error al realizar el query'));

      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);
      dynamodbAPP.__set__('getError', errorSpy);

      try {
        await query(params, 'desc');
      } catch (e) {
        assert.equal(e.message, 'Error al realizar el query');
      }
    });
  });
});
