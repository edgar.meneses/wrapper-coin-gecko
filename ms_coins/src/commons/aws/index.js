const AWS = require('aws-sdk');
const config = require('../../startup/config');

if (config.env === 'local') {
  AWS.config.update({
    region: config.aws.region,
    endpoint: config.aws.endpoint,
  });
} else {
  AWS.config.update({
    region: config.aws.region,
  });
}

AWS.config.apiVersions = {
  dynamodb: config.aws.dynamoDB.apiVersion,
};

module.exports = AWS;
