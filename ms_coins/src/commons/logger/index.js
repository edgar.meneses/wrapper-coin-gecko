const { createLogger, format, transports } = require('winston');

const index = createLogger({
  level: 'verbose',
  format: format.combine(
    format.timestamp(),
    format.json(),
    format.prettyPrint(),
  ),
  defaultMeta: { service: 'wrapper-coin-gecko' },
  transports: [
    new transports.File({ filename: 'error.log', level: 'error' }),
    new transports.File({ filename: 'combine.log' }),
    new transports.Console(),
  ],
});

module.exports = index;
