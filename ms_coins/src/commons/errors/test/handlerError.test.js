const { assert } = require('chai');
const {
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../wrapperError');
const errorList = require('../errorList');

let sandbox;
let getError;
let getInfoErrorFromStatus;
let handlerErrorApp;
let getRequestError;

describe('Handler error test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
    handlerErrorApp = rewire('../handlerError.js');
    getError = handlerErrorApp.__get__('getError');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('get error ', () => {
    it('Should return wrapper error when key error exists in errorList.json', () => {
      const expectedValues = errorList.errors.loadConvictFile;

      const newError = getError(new Error(), 'loadConvictFile');

      assert.equal(newError.code, expectedValues[0]);
      assert.equal(newError.status, expectedValues[1]);
      assert.equal(newError.message, expectedValues[2]);
    });

    it('Should return default error when key error do not exists in errorList.json', () => {
      const newError = getError(new Error(), 'noKey');

      assert.equal(newError.code, 'WCG-000');
      assert.equal(newError.status, 500);
      assert.equal(newError.message, 'Error undefined');
    });

    it('Should return param error when error is instanceOf WrapperError', () => {
      const wrapperError = new WrapperError('WCG-020', 401, 'No esta autorizado', new Error());

      const newError = getError(wrapperError);

      assert.deepEqual(newError, wrapperError);
    });
  });

  describe('Get information  error from status code', () => {
    beforeEach(() => {
      getInfoErrorFromStatus = handlerErrorApp.__get__('getInfoErrorFromStatus');
    });

    it('Should return bad request info when status code is 400', () => {
      const expected = {
        statusCode: 400,
        title: 'Bad Request',
        url: 'https://tools.ietf.org/html/rfc7231#section-6.5.1',
      };

      const getInfo = getInfoErrorFromStatus(400);

      assert.deepEqual(getInfo, expected);
    });

    it('Should return unauthorized info when status code is 401', () => {
      const expected = {
        statusCode: 401,
        title: 'Unauthorized',
        url: 'https://tools.ietf.org/html/rfc7231#section-3.1',
      };

      const getInfo = getInfoErrorFromStatus(401);

      assert.deepEqual(getInfo, expected);
    });

    it('Should return forbidden info when status code is 403', () => {
      const expected = {
        statusCode: 403,
        title: 'Forbidden',
        url: 'https://tools.ietf.org/html/rfc7231#section-6.5.3',
      };

      const getInfo = getInfoErrorFromStatus(403);

      assert.deepEqual(getInfo, expected);
    });

    it('Should return Not found info when status code is 404', () => {
      const expected = {
        statusCode: 404,
        title: 'Not Found',
        url: 'https://tools.ietf.org/html/rfc7231#section-6.5.4',
      };

      const getInfo = getInfoErrorFromStatus(404);

      assert.deepEqual(getInfo, expected);
    });

    it('Should return Internal server error info when status code is 500', () => {
      const expected = {
        statusCode: 500,
        title: 'Internal Server Error',
        url: 'https://tools.ietf.org/html/rfc7231#section-6.6.1',
      };

      const getInfo = getInfoErrorFromStatus(500);

      assert.deepEqual(getInfo, expected);
    });

    it('Should return service unavailable server error info when status code is 500', () => {
      const expected = {
        statusCode: 503,
        title: 'Service Unavailable',
        url: 'https://tools.ietf.org/html/rfc7231#section-6.6.4',
      };

      const getInfo = getInfoErrorFromStatus(503);

      assert.deepEqual(getInfo, expected);
    });
  });

  describe('Get request error', () => {
    beforeEach(() => {
      getRequestError = handlerErrorApp.__get__('getRequestError');
    });

    it('Should return error with request structure when error.status exists', () => {
      const wrapperError = new WrapperError('WCG09', 403, 'Error forbidden', null, { method: 1 });
      const uuidSpy = stub();
      uuidSpy.returns('1');
      const getInfoStatus = stub();
      getInfoStatus.returns({ title: 'Forbidden', url: 'https://' });

      handlerErrorApp.__set__('uuid', uuidSpy);
      handlerErrorApp.__set__('getInfoErrorFromStatus', getInfoStatus);

      const getData = getRequestError(wrapperError);

      assert.deepEqual(getData, {
        error: {
          id: '1',
          href: 'https://',
          status: 403,
          code: wrapperError.code,
          title: 'Forbidden',
          detail: wrapperError.message,
          trace: wrapperError.detail,
        },
      });
    });

    it('Should return error with request structure when error.status do not exits', () => {
      const uuidSpy = stub();
      uuidSpy.returns('1');
      const getInfoStatus = stub();
      getInfoStatus.returns({ title: 'Internal', url: 'https://1' });

      handlerErrorApp.__set__('uuid', uuidSpy);
      handlerErrorApp.__set__('getInfoErrorFromStatus', getInfoStatus);

      const getData = getRequestError({
        code: 'WCG0091',
        message: 'Error interno',
        detail: { method: '2' },
      });

      assert.deepEqual(getData, {
        error: {
          id: '1',
          href: 'https://1',
          status: 500,
          code: 'WCG0091',
          title: 'Internal',
          detail: 'Error interno',
          trace: { method: '2' },
        },
      });
    });

    it('Should return error with request structure when error.code and '
      + 'error.message do not exits', () => {
      const uuidSpy = stub();
      uuidSpy.returns('1');
      const getInfoStatus = stub();
      getInfoStatus.returns({ title: 'Not found', url: 'https://2' });

      handlerErrorApp.__set__('uuid', uuidSpy);
      handlerErrorApp.__set__('getInfoErrorFromStatus', getInfoStatus);

      const getData = getRequestError({ status: 404, detail: { method: '2' } });

      assert.deepEqual(getData, {
        error: {
          id: '1',
          href: 'https://2',
          status: 404,
          code: 'NOT DEFINED',
          title: 'Not found',
          detail: 'NOT DEFINED',
          trace: { method: '2' },
        },
      });
    });
  });
});
