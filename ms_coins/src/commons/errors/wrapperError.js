// eslint-disable-next-line func-names
const WrapperError = function (code, status, message, error, detail) {
  this.code = code;
  this.status = status;
  this.message = message;
  this.stack = error ? error.stack : new Error().stack;
  this.error = error;
  this.detail = detail;
};

WrapperError.prototype = Object.create(Error.prototype);
WrapperError.prototype.constructor = WrapperError;

module.exports = WrapperError;
