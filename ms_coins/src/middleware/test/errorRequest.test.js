const { assert } = require('chai');
const {
  spy,
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');

let sandbox;
let res;
let errorRequestApp;
let errorMiddleware;
let notFoundMiddleware;

describe('Error request middleware', () => {
  beforeEach(() => {
    sandbox = createSandbox();

    res = {
      status: stub(),
      json: stub(),
    };
    res.status.returns(res);

    errorRequestApp = rewire('../errorRequest.js');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Error middleware', () => {
    beforeEach(() => {
      errorMiddleware = errorRequestApp.__get__('errorMiddleware');
    });

    it('Should return error response', async () => {
      const infoError = {
        error: {
          status: 500,
          message: 'No es posible consultar la información',
          href: 'https://2',
        },
      };

      const requestErrorSpy = stub();
      requestErrorSpy.returns(infoError);
      errorRequestApp.__set__('getRequestError', requestErrorSpy);
      errorRequestApp.__set__('error', spy());

      await errorMiddleware(spy(), spy(), res, spy());

      assert.deepEqual(res.json.args[0][0], infoError);
    });
  });

  describe('Not found middleware', () => {
    beforeEach(() => {
      notFoundMiddleware = errorRequestApp.__get__('notFoundMiddleware');
    });

    it('Should return not found error response', async () => {
      const infoError = {
        error: {
          status: 404,
          message: 'Not found',
          href: 'https://2',
        },
      };

      const notFoundSpy = stub();
      notFoundSpy.returns(infoError);

      const getErrorSpy = stub();
      getErrorSpy.returns(['WC001', 404, 'Not found']);

      errorRequestApp.__set__('getRequestError', notFoundSpy);
      errorRequestApp.__set__('error', spy());
      errorRequestApp.__set__('getError', getErrorSpy);

      await notFoundMiddleware(spy(), res);

      assert.deepEqual(res.json.args[0][0], infoError);
    });
  });
});
