const { validationResult } = require('express-validator');
const { getError } = require('../commons/errors/handlerError');

const sanitization = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    next(getError(null, 'sanitization', {
      method: 'sanitization',
      errors: errors.array(),
    }));
  } else {
    next();
  }
};

module.exports = sanitization;
