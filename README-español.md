# Wrapper para el API de CoinGecko

En este proyecto encontrara algunas funciones que permiten interactuar con el API de CoinGecko mediante
 el cual usted obtendrá información sobre las criptomnedas así como su respectivo valor en USD , EUR y ARS


Con este wrapper usted podra registrarse, conocer el listado de criptomonedas existentes y generar una 
cotización de acuerdo a la moneda favorita.

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

Para la instalación de wrapper-coin-gecko requiere tener instalado:

> **NodeJs**
>
> La instalación depende del SO en ubuntu podría utilizar 
>
> ```
> $ sudo apt install nodejs
> ```
>
> para mayor información visite [Node](https://nodejs.org/es/download/)
> 
>
> **DynamoDB local**
> 
> Existen dos maneras de instalar DynamoDB en la maquina local. Puede seleccionar cualquiera de las dos
> 
>> Descargar y ejecutar binario DynamoDBLocal.jar
>> ```
>>  $ java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb
>>  ```
>> Para mayor informacón visite [DynamoDB-local](https://docs.aws.amazon.com/es_es/amazondynamodb/latest/developerguide/DynamoDBLocal.DownloadingAndRunning.html)
>>
>> Ejecutar con Docker 
>> Debe descargar e instalar [Docker-Desktop](https://www.docker.com/products/docker-desktop) y crear un archivo 
>>  docker-compose.yml con el siguente código
>> ```
>> version: '3.7'
>>   services:
>>    dynamodb-local:
>>      image: amazon/dynamodb-local:latest
>>      container_name: dynamodb-local
>>      ports:
>>       - "8000:8000"
>> ```
>> 
>> **git** 
>> 
>> puede instalar git en ubuntu utilizando
>> ```
>> $ sudo apt install git
>> ```
>> Para instalar en otros SO o para mayor información visite [git](https://git-scm.com/downloads)
>>
>> **AWS-cli**
>> puede instalar AWS cli en ubuntu ejecutando 
>>
>> ```
 >> $ pip3 install awscli --upgrade --user
 >> ```
>> para mayor inforamción visite [AWS - v1](https://docs.aws.amazon.com/es_es/cli/latest/userguide/install-cliv1.html)
>>
>>
>
>

### Instalación 🔧

A contuniación encontrará una serie de pasos para la instalación y ejecución del proyecto 

**Paso 1**

Descargue el repositorio

```
git clone https://gitlab.com/edgar.meneses/crud-provider.git
```

**Paso 2**

Instale las dependencias para el microservicio de oauth

```
$ cd ms_oauth/
$ npm install
```

**Paso 3**

Configure los secrets de la aplicación

```
$ nano .env
>  SECRET_TOKEN=a0ncL0Rh4XAnnHgPoTTUDgs5oZpEBW2D
>  OAUTH_CLIENT_ID=AKIAJII2LE4L5JSV76NQ
>  OAUTH_SECRET_ID=ZDc2VsgBB154y53aqVD8k9wYFiBNf4f/mgLtVcDc
>  USER_SECRET_ID=M9oOOoDakuxXPypeyvlm6B/iaTZb4GBGcKlnCjPh
>  USER_CLIENT_ID=AKIAI3S4VMLFA5QX3TNQ
>  CURRENCY_SECRET_ID=3wBunDNfsZMioyUP+/+r/eWabtssbZFRGOK8q34v
>  CURRENCY_CLIENT_ID=AKIAJAVBZ6ZEDBV2UBOA
```
Los el valor de los secrets descritos enteriomente son de referencia usted podra utilizar otros valores si asi lo desea.
 mira **Deployment** para mas detalles. 

**Paso 4**

Ejecute el proyecto de acuerdo al ambiente que se va a desplegar la solucion

> ****Envirolments****
>
> Este proyecto esta desarrollado para ejecutarse en diferentes ambientes de acuerdo a un archivo de configuracón
>
> ******local******
>
> Este ambiente es la maquina local de cada desarrollador. Las conexiones se realizan en el mismo dispositivo 
> generalmente utilizando *`locahost`* o *`127.0.0.1`*
>
>  ******dev******
>
> Ambiente para el equipo de desarrollo. Contiene conexiones con los servidores onpremise o servicios cloud
> 
>  ******qa******
>
> Ambiente para el equipo de certificación. Contiene conexiones con los servidores onpremise o servicios cloud y es un
> ambiente muy estable y con configuraciónes similares a las de producción 
>
>   ******pdn******
 >
 > Ambiente productivo unicamente para clientes.
 > 
```
$ ENVIRONMENT=local npm start
```

**Paso 5**

Cree los modelos de BD

Para crear las dos tablas *`users-wcg-local`* y *`coins-wcg-local`* ubique el directorio *`docs`*
en el respositorio. Ubiquese en la carpeta *`db`* y las sentencias aws para crear cada tabla

```
$ cd docs/db/

$ aws dynamodb create-table --cli-input-json file://user.json --endpoint-url http://localhost:8000
$ aws dynamodb create-table --cli-input-json file://coins.json --endpoint-url http://localhost:8000

```
 mira **Deployment** para mas detalles.
 
 Repita los pasos anteriores para los otros dos microservicios exceptuando el **Paso 5**

```
$ cd ms_user/
$ cd ms_coins/
```

Podra comprobar que los servicios esten corriendo exitosamente realizando una peticion al servicio de healtcheck 
de cada microservicio

```
$ curl --location --request GET 'localhost:9091/wrapper-coin-gecko/v1/health'
$ curl --location --request GET 'localhost:9092/wrapper-coin-gecko/v1/health'
$ curl --location --request GET 'localhost:9093/wrapper-coin-gecko/v1/health'
```

debera obtener una respuesta similar a la siguente:
```
{
    "data": {
        "message": "Server running success"
    }
}
```

## Ejecutando las pruebas ⚙️

En construcción

### Analice las pruebas end-to-end 🔩

En construcción

### Y las pruebas de estilo de codificación ⌨️

En construcción

## Despliegue 📦

En construcción

## Construido con 🛠️

Este proyecto fue construido utilizando:

* [NodeJs]() - Entorno de desarrollo para JavaScript 
* [npm]() - Manejador de dependencias
* [express]() - Usado como framework de desarrollo
* [convict]() - Configurar ambientes del rpuecto
* [aws-sdk]() - Para conexion con BD
* [mocha]() - Entorno para pruebas unitarias
* [sinon]() - Espiar funciones en UT
* [eslint]() - Usado para implementar una guia de estilos 
* [arbnb]() - Guia de estilos implementada

## Contribuyendo 🖇️ 

Por favor lee el [CONTRIBUTING.md]() para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki]()

## Autores ✒️

* **Edgar Antonio Meneses Cadena** - *Trabajo Inicial* - [emeneses](https://gitlab.com/edgar.meneses)

## Licencia 📄

Este proyecto está bajo la Licencia MIT - mira el archivo [LICENSE.md](LICENSE.md) para detalles


---
⌨️ con ❤️ por [emeneses](https://gitlab.com/edgar.meneses) 😊
