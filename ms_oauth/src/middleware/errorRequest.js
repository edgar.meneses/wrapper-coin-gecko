const { error } = require('../commons/logger');
const WrapperError = require('../commons/errors/wrapperError');
const { getRequestError, getError } = require('../commons/errors/handlerError');

// eslint-disable-next-line no-unused-vars
const errorMiddleware = (err, req, res, next) => {
  // error(err);

  const infoError = getRequestError(err);
  res.status(infoError.error.status).json(infoError);
};

const notFoundMiddleware = (req, res) => {
  const notFoundError = new WrapperError(getError('urlNotFound'));

  const infoError = getRequestError(notFoundError);

  res.status(404).json(infoError);
};

module.exports = {
  errorMiddleware,
  notFoundMiddleware,
};
