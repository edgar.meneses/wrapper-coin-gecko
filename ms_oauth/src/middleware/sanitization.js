const { validationResult } = require('express-validator');
const { getError } = require('../commons/errors/handlerError');

const sanitization = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    next(getError(null, 'userInvalid'));
  } else {
    next();
  }
};

module.exports = sanitization;
