require('dotenv').config();
const convict = require('convict');
const schema = require('./config/schema');
const { getError } = require('../commons/errors/handlerError');
const { error } = require('../commons/logger');

const getConfig = () => {
  try {
    const config = convict(schema);
    config.loadFile(`./src/startup/config/environment/${config.get('env')}.json`);
    config.validate({ strict: true });
    return config;
  } catch (e) {
    const env = process.env.ENVIRONMENT;
    const wrapperError = getError(e, 'loadConvictFile',
      { method: 'getConfig', env });
    error(wrapperError);
    throw wrapperError;
  }
};

const convictConfig = getConfig();
const config = {
  env: convictConfig.get('env'),
  port: convictConfig.get('port'),
  userTableName: convictConfig.get('userTableName'),
  aws: convictConfig.get('aws'),
  roundSalt: 10,
  userRequestURL: convictConfig.get('userRequestURL'),
  jwt: convictConfig.get('jwt'),
  secretsAPP: convictConfig.get('secretsAPP'),
};

module.exports = config;
