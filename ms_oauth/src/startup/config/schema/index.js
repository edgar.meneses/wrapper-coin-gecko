const schema = {
  env: {
    doc: 'It is the application execution environment variable.',
    format: ['local', 'dev', 'qa', 'pdn'],
    env: 'ENVIRONMENT',
    default: 'local',
  },
  port: {
    doc: 'It is the port of execution the application',
    format: Number,
    env: 'PORT',
    default: 9091,
  },
  userTableName: {
    doc: 'It is name of dynamoDB table to save user data',
    format: String,
    default: '',
  },
  aws: {
    region: {
      doc: 'It is the aws region',
      format: String,
      default: 'local',
    },
    endpoint: {
      doc: 'It is host where the dynamodb server is hosted',
      format: 'String',
      default: 'localhost:8080',
    },
    dynamoDB: {
      apiVersion: {
        doc: 'It is dynamodb version of API',
        format: String,
        default: '',
      },
    },
  },
  userRequestURL: {
    doc: 'It is the URL of ms_user',
    format: 'String',
    default: 'localhost:9091',
  },
  jwt: {
    secret: {
      doc: 'It is the secret to create token',
      format: String,
      default: '',
      env: 'SECRET_TOKEN',
    },
    expireIn: {
      doc: 'It is time expiration secret',
      format: Number,
      default: 60,
    },
  },
  secretsAPP: {
    'ms_oauth:WCG': {
      clientId: {
        doc: 'It is the secret id of microservice oauth',
        format: String,
        default: '',
        env: 'OAUTH_CLIENT_ID',
      },
      clientSecret: {
        doc: 'It is the client secret of microservice oauth',
        format: String,
        default: '',
        env: 'OAUTH_SECRET_ID',
      },
    },
    'ms_user:WCG': {
      clientId: {
        doc: 'It is the secret id of microservice oauth',
        format: String,
        default: '',
        env: 'USER_CLIENT_ID',
      },
      clientSecret: {
        doc: 'It is the client secret of microservice oauth',
        format: String,
        default: '',
        env: 'USER_SECRET_ID',
      },
    },
    'ms_currency:WCG': {
      clientId: {
        doc: 'It is the secret id of microservice oauth',
        format: String,
        default: '',
        env: 'CURRENCY_CLIENT_ID',
      },
      clientSecret: {
        doc: 'It is the client secret of microservice oauth',
        format: String,
        default: '',
        env: 'CURRENCY_SECRET_ID',
      },
    },
  },
};

module.exports = schema;
