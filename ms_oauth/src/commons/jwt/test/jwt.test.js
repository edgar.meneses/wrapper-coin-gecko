const { assert } = require('chai');
const {
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');

let sandbox;
let generateToken;
let serviceApp;
let config;
let verifyToken;

describe('Service oauth test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();

    config = {
      jwt: {
        expireIn: 60,
        secret: 'misecret',
      },
    };
    serviceApp = rewire('../index.js');
    serviceApp.__set__('config', config);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Generate token', () => {
    beforeEach(() => {
      generateToken = serviceApp.__get__('generateToken');
    });

    it('should return jwt token when function run success', async () => {
      const jwt = {
        sign: (payload, secret, options, callback) => {
          callback(null, '$2b$10$7');
        },
      };

      serviceApp.__set__('jwt', jwt);

      const token = await generateToken({ username: 'emeneses' });

      assert.equal(token, '$2b$10$7');
    });

    it('should return error when jwt.sign token fail', () => {
      const jwt = {
        sign: (payload, secret, options, callback) => {
          callback(new Error());
        },
      };
      const getErrorSpy = stub();
      getErrorSpy.returns(new Error('error al generar el token'));

      serviceApp.__set__('jwt', jwt);
      serviceApp.__set__('getError', getErrorSpy);

      try {
        generateToken({ username: 'emeneses' });
      } catch (e) {
        assert.equal(e.message, 'error al generar el token');
      }
    });
  });

  describe('Verify token', () => {
    beforeEach(() => {
      verifyToken = serviceApp.__get__('verifyToken');
    });

    it('Should return token payload when token is valid', async () => {
      const decodeToken = { username: 'emeneses', iat: 1597530832, exp: 1597530892 };
      const jwt = {
        verify: (token, secret, callback) => {
          callback(null, decodeToken);
        },
      };

      serviceApp.__set__('jwt', jwt);

      const tokenData = await verifyToken('kl40UapMvzY');

      assert.deepEqual(tokenData, decodeToken);
    });

    it('Should return undefined when token is invalid', async () => {
      const jwt = {
        verify: (token, secret, callback) => {
          callback(null);
        },
      };

      serviceApp.__set__('jwt', jwt);

      const tokenData = await verifyToken('kl40UapMvzY');

      assert.isUndefined(tokenData);
    });

    it('Should return error when verify token failed', async () => {
      const jwt = {
        verify: (payload, secret, callback) => {
          callback(new Error());
        },
      };
      const getErrorSpy = stub();
      getErrorSpy.returns(new Error('error varificando el token'));

      serviceApp.__set__('jwt', jwt);
      serviceApp.__set__('getError', getErrorSpy);

      try {
        verifyToken('12ada1');
      } catch (e) {
        assert.equal(e.message, 'error verificando el token');
      }
    });
  });
});
