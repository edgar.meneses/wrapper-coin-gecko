const jwt = require('jsonwebtoken');
const config = require('../../startup/config');
const { getError } = require('../errors/handlerError');

const generateToken = (payload) => new Promise((resolve, reject) => {
  jwt.sign(
    payload,
    config.jwt.secret,
    { expiresIn: config.jwt.expireIn },
    (err, token) => {
      if (err) {
        reject(getError(err, 'signToken',
          { method: 'generateToken' }));
      } else {
        resolve(token);
      }
    },
  );
});

const verifyToken = (token) => new Promise((resolve, reject) => {
  jwt.verify(token, config.jwt.secret, (err, decode) => {
    if (err) {
      reject(getError(err, 'verifyToken'), { method: 'verifyToken' });
    } else {
      resolve(decode);
    }
  });
});

module.exports = {
  generateToken,
  verifyToken,
};
