const { assert } = require('chai');
const {
  spy,
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../errors/wrapperError');

let sandbox;
let requestApp;
let httpGetRequest;

describe('Request test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
    requestApp = rewire('../index');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('http get request', () => {
    beforeEach(() => {
      httpGetRequest = requestApp.__get__('httpGetRequest');
    });

    it('Should return data when http request is success', async () => {
      const info = { name: 'Edgar' };
      const axios = {
        get: () => ({ data: info }),
      };

      requestApp.__set__('axios', axios);

      const requestdata = await httpGetRequest('https://1');

      assert.deepEqual(requestdata, info);
    });

    it('Should return error when http request is fail', async () => {
      const errorData = ['WC78', 500, 'Error al consultar'];
      const axios = {
        get: () => (Promise.reject(new Error())),
      };
      const errSpy = stub();
      errSpy.returns(new WrapperError(...errorData));

      requestApp.__set__('axios', axios);
      requestApp.__set__('getError', errSpy);

      try {
        await httpGetRequest('https://1');
      } catch (e) {
        assert.deepEqual(e.code, errorData[0]);
        assert.deepEqual(e.status, errorData[1]);
        assert.deepEqual(e.message, errorData[2]);
      }
    });

    it('Should return error when user is invalid', async () => {
      const errorData = ['WC79', 401, 'Usuario invalido'];
      const axios = {
        get: () => (Promise.reject(new Error())),
      };
      const errSpy = stub();
      errSpy.returns(new WrapperError(...errorData));

      requestApp.__set__('axios', axios);
      requestApp.__set__('getError', errSpy);

      try {
        await httpGetRequest('https://1', 'userInvalid');
      } catch (e) {
        assert.deepEqual(e.code, errorData[0]);
        assert.deepEqual(e.status, errorData[1]);
        assert.deepEqual(e.message, errorData[2]);
      }
    });
  });
});
