const axios = require('axios');
const { getError } = require('../errors/handlerError');

const httpGetRequest = async (url, errorName, params, headers) => {
  try {
    const config = {
      responseType: 'json',
      params,
      headers,
    };
    const responseData = await axios.get(url, config);
    return responseData.data;
  } catch (e) {
    console.log(e);
    const detail = (errorName === 'userInvalid')
      ? { method: 'login' }
      : { method: `get request - ${errorName}` };
    throw getError(e, errorName, detail);
  }
};

module.exports = {
  httpGetRequest,
};
