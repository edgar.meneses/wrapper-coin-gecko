const { assert } = require('chai');
const {
  spy,
  stub,
  createSandbox,
} = require('sinon');

const WrapperError = require('../wrapperError');

const error = new Error();
let sandbox;

describe('Wrapper Error test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Create WrapperError', () => {
    it('Should return code, status and message when new WrapperError is created', () => {
      const errorData = {
        code: 'WCG-400',
        status: 400,
        message: 'El campo es requerido',
      };

      const wrapperError = new WrapperError(errorData.code, errorData.status, errorData.message);

      assert.equal(wrapperError.code, errorData.code);
      assert.equal(wrapperError.status, errorData.status);
      assert.equal(wrapperError.message, errorData.message);
      assert.isUndefined(wrapperError.error);
      assert.isUndefined(wrapperError.detail);
    });

    it('Should return code, status, message and error when new WrapperError is created and error is passed', () => {
      const errorData = {
        code: 'WCG-404',
        status: 404,
        message: 'Recurso no encontrado',
      };

      const wrapperError = new WrapperError(errorData.code, errorData.status,
        errorData.message, error);

      assert.equal(wrapperError.code, errorData.code);
      assert.equal(wrapperError.status, errorData.status);
      assert.equal(wrapperError.message, errorData.message);
      assert.equal(wrapperError.error, error);
      assert.isUndefined(wrapperError.detail);
    });

    it('Should return code, status, message, error and detail '
      + 'when new WrapperError is created and error and detail are passed', () => {
      const errorData = {
        code: 'WCG-500',
        status: 404,
        message: 'Error interno',
        detail: { option: 'findUser' },
      };

      const wrapperError = new WrapperError(errorData.code, errorData.status,
        errorData.message, error, errorData.detail);

      assert.equal(wrapperError.code, errorData.code);
      assert.equal(wrapperError.status, errorData.status);
      assert.equal(wrapperError.message, errorData.message);
      assert.equal(wrapperError.error, error);
      assert.deepEqual(wrapperError.detail, errorData.detail);
    });
  });
});
