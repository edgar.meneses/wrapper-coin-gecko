const { assert } = require('chai');
const {
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../errors/wrapperError');

let sandbox;
let encriptApp;
let verifyPassword;

describe('Encript password test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
    encriptApp = rewire('../index.js');

    verifyPassword = encriptApp.__get__('verifyPassword');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('verify password', () => {
    it('Should return true when hash is valid', async () => {
      const hashResult = await verifyPassword('abc123', '$2b$10$7/Bm0HYuC9yQ8ggSbqCRW.3uUxBXlizgFW.hHzu0Tlf74aleBiZri');

      assert.isTrue(hashResult);
    });

    it('Should return false when hash is valid', async () => {
      const hashResult = await verifyPassword('abc1234', '$2b$10$7/Bm0HYuC9yQ8ggSbqCRW.3uUxBXlizgFW.hHzu0Tlf74aleBiZri');

      assert.isFalse(hashResult);
    });

    it('Should return false when hash and password are empty', async () => {
      const hashResult = await verifyPassword();

      assert.isFalse(hashResult);
    });

    it('Should return error when bcrypt.compare fail', async () => {
      const getError = stub();
      getError.returns(new WrapperError('WCG012', 500, 'Error al verificar la contraseña'));

      encriptApp.__set__('getError', getError);

      try {
        await verifyPassword('abc', '$2b$10$7/Bm0HYuC9yQ8ggSbqCRW.3uUxBXlizgFW.hHzu0Tlf74al');
      } catch (e) {
        assert.equal(e.code, 'WC012');
        assert.equal(e.status, 500);
        assert.equal(e.message, 'Error al verificar la contraseña');
      }
    });

    it('Should return Wrapper Error when bcrypt.compare fail', async () => {
      try {
        encriptApp.__set__('bcrypt', {
          compare: Promise.reject(),
        });
        await verifyPassword('abc', '$<<<l');
      } catch (e) {
        assert.equal(e.code, 'WCG-OAH-106');
        assert.equal(e.status, 500);
        assert.equal(e.message, 'No es posible verificar la información del usuario');
      }
    });
  });
});
