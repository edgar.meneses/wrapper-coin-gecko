const bcrypt = require('bcrypt');
const { getError } = require('../errors/handlerError');

const verifyPassword = async (password = '', hash = '') => {
  try {
    const result = await bcrypt.compare(password, hash);
    return result;
  } catch (e) {
    throw getError(e, 'verifyPassword', { method: 'verify password' });
  }
};

module.exports = {
  verifyPassword,
};
