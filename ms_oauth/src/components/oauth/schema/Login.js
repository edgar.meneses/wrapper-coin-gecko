const { body } = require('express-validator');

const User = [
  body('username')
    .notEmpty()
    .withMessage('El campo username es obligatorio')
    .isAlphanumeric()
    .withMessage('El campo username no contiene un valor valido')
    .isLength({ min: 5, max: 50 })
    .withMessage('El campo username no contiene una longitud valida'),
  body('password')
    .notEmpty()
    .withMessage('El campo password es obligatorio')
    .isAlphanumeric()
    .withMessage('El campo password no contiene un valor valido')
    .isLength({ min: 8 })
    .withMessage('El campo passwod no contiene una longitud valida'),
];

module.exports = User;
