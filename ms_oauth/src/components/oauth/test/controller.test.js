const { assert } = require('chai');
const {
  spy,
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../../commons/errors/wrapperError');

let sandbox;
let req;
let res;
let controllerApp;
let login;
let next;
let verifyToken;

describe('oAuth controller', () => {
  beforeEach(() => {
    sandbox = createSandbox();

    res = {
      status: stub(),
      json: stub(),
    };
    res.status.returns(res);

    next = spy();

    controllerApp = rewire('../controller.js');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('login', () => {
    beforeEach(() => {
      req = {
        body: {
          username: 'emeneses',
        },
      };

      login = controllerApp.__get__('login');
    });

    it('Should return token when user is valid', async () => {
      const tokenResponse = {
        data: {
          token: 'fklm09',
        },
      };

      controllerApp.__set__('findUser', () => ({
        data: {
          userInfo: { password: '121111' },
        },
      }));
      controllerApp.__set__('loginService', () => (tokenResponse.data));

      const next = spy();

      await login(req, res, next);

      assert.deepEqual(res.json.args[0][0], tokenResponse);
    });

    it('Should return error when user is invalid', async () => {
      const errorSpry = stub();
      errorSpry.returns(new WrapperError('WCG090', 401, 'User invalid'));

      controllerApp.__set__('findUser', () => ({ password: '121111' }));
      controllerApp.__set__('loginService', () => ({ invalidUser: true }));
      controllerApp.__set__('getError', errorSpry);

      await login(req, res, next);

      assert.equal(next.args[0][0].code, 'WCG090');
      assert.equal(next.args[0][0].status, 401);
      assert.equal(next.args[0][0].message, 'User invalid');
    });

    it('Should return userInvalid error when user is invalid', async () => {
      controllerApp.__set__('findUser', () => ({
        data: {
          userInfo: { password: '121111' },
        },
      }));
      controllerApp.__set__('loginService', () => ({ invalidUser: true }));

      await login(req, res, next);

      assert.equal(next.args[0][0].code, 'WCG-OAH-109');
      assert.equal(next.args[0][0].status, 401);
      assert.equal(next.args[0][0].message, 'El usuario o la contraseña no son validos');
    });

    it('Should return error when findUser fail', async () => {
      const errorData = ['WCG89', 500, 'Error consultando el usuario'];
      const errorSpry = stub();
      errorSpry.returns(new WrapperError(...errorData));

      controllerApp.__set__('findUser', () => (Promise.reject(new Error())));
      controllerApp.__set__('loginService', () => ({ }));
      controllerApp.__set__('getError', errorSpry);

      await login(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });

    it('Should return error when loginService fail', async () => {
      const errorData = ['WCG9', 500, 'Error al generar el token'];
      const errorSpry = stub();
      errorSpry.returns(new WrapperError(...errorData));

      controllerApp.__set__('findUser', () => false);
      controllerApp.__set__('loginService', () => ((Promise.reject(new Error()))));
      controllerApp.__set__('getError', errorSpry);

      await login(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });
  });

  describe('Verify Token', () => {
    beforeEach(() => {
      verifyToken = controllerApp.__get__('verifyToken');
    });

    it('Should payload when token is valid and authorization method is Bearer', async () => {
      const userPayload = { id: '1234', username: 'emeneses' };
      req = {
        get: () => ('Bearer Token'),
      };

      controllerApp.__set__('isValidToken', () => (userPayload));

      await verifyToken(req, res, next);

      assert.deepEqual(res.json.args[0][0].data, { payload: { ...userPayload } });
    });

    it('Should error when is isValidToken fail and authorization method is Bearer', async () => {
      const errorData = ['WC00', 401, 'Token expired'];
      req = {
        get: () => ('Bearer Token'),
      };

      controllerApp.__set__('isValidToken', () => (Promise.reject(new Error())));
      controllerApp.__set__('getError', () => (new WrapperError(...errorData)));

      await verifyToken(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });

    it('Should return success basic validation when sectId and secretClient are valid'
      + 'and authorization method is Basic', async () => {
      req = {
        get: () => ('Basic application'),
      };

      controllerApp.__set__('isValidBasicAuthorization', () => true);

      await verifyToken(req, res, next);

      assert.deepEqual(res.json.args[0][0].data, {
        payload: {
          authorization: true,
          application: 'application',
        },
      });
    });

    it('Should return error basic validation when sectId and secretClient are not valid'
      + 'and authorization method is Basic', async () => {
      const errorData = ['WC0091', 401, 'Basic token is invalid'];
      req = {
        get: () => ('Basic application'),
      };

      controllerApp.__set__('isValidBasicAuthorization', () => false);
      controllerApp.__set__('getError', () => (new WrapperError(...errorData)));

      await verifyToken(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });

    it('Should return error  when isValidBasicAuthorization fail '
      + 'and authorization method is Basic', async () => {
      const errorData = ['WC0092', 500, 'Basic token is invalid'];
      req = {
        get: () => ('Basic application'),
      };

      controllerApp.__set__('isValidBasicAuthorization', () => { throw new Error(); });
      controllerApp.__set__('getError', () => (new WrapperError(...errorData)));

      await verifyToken(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });

    it('Should return error  when Authorization method is not valid', async () => {
      const errorData = ['WC0093', 401, 'Metodo invalido'];
      req = {
        get: () => ('Mutual application'),
      };

      controllerApp.__set__('isValidBasicAuthorization', () => { throw new Error(); });
      controllerApp.__set__('getError', () => (new WrapperError(...errorData)));

      await verifyToken(req, res, next);

      assert.equal(next.args[0][0].code, errorData[0]);
      assert.equal(next.args[0][0].status, errorData[1]);
      assert.equal(next.args[0][0].message, errorData[2]);
    });
  });
});
