const { assert } = require('chai');
const {
  spy,
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../../commons/errors/wrapperError');

let sandbox;
let findUser;
let serviceApp;
let login;
let isValidToken;
let isValidBasicAuthorization;

describe('oauth service test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
    serviceApp = rewire('../service.js');
    serviceApp.__set__('config', {
      userRequestURL: 'https://1',
    });
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Find user', () => {
    beforeEach(() => {
      findUser = serviceApp.__get__('findUser');
    });

    it('Should return user when this exist', async () => {
      const user = {
        username: 'emeneses',
      };
      serviceApp.__set__('config', {
        secretsAPP: {
          'ms_oauth:WCG': {
            clientSecret: '1',
            clientId: '1',
          },
        },
      });
      serviceApp.__set__('httpGetRequest', () => user);

      const userFind = await findUser('user23');

      assert.deepEqual(userFind, user);
    });

    it('Should return error when user not exist', async () => {
      const getError = stub();
      getError.returns(new WrapperError('WC00', 400, 'error al buscar el usuario'));
      serviceApp.__set__('config', spy());
      serviceApp.__set__('httpGetRequest', () => new Error());
      serviceApp.__set__('getError', getError);

      try {
        await findUser('user23');
      } catch (e) {
        console.log(e);
        assert.equal(e.code, 'WC00');
        assert.equal(e.status, 400);
        assert.equal(e.message, 'error al buscar el usuario');
      }
    });

    it('Should return error when findUser fail', async () => {
      const errorData = ['WC01', 500, 'Error al consultar el usuario'];
      serviceApp.__set__('httpGetRequest', () => new Error());
      serviceApp.__set__('getError', () => (new WrapperError(...errorData)));

      try {
        await findUser('user23');
      } catch (e) {
        assert.equal(e.code, errorData[0]);
        assert.equal(e.status, errorData[1]);
        assert.equal(e.message, errorData[2]);
      }
    });

    it('Should return error when httpGetRequest fail', async () => {
      const errorData = ['WCG-OAH-109', 401, 'El usuario o la contraseña no son validos'];

      try {
        await findUser('user23');
      } catch (e) {
        assert.equal(e.code, errorData[0]);
        assert.equal(e.status, errorData[1]);
        assert.equal(e.message, errorData[2]);
      }
    });
  });

  describe('Login user', () => {
    beforeEach(() => {
      login = serviceApp.__get__('login');
    });

    it('Should return token when verify password is success', async () => {
      const tokenSpy = stub();
      tokenSpy.returns('token1');

      serviceApp.__set__('verifyPassword', () => true);
      serviceApp.__set__('generateToken', tokenSpy);

      const token = await login(spy(), spy());

      assert.deepEqual(token, { token: 'token1' });
    });

    it('Should return invalid usuer when verify password fail', async () => {
      const tokenSpy = stub();
      tokenSpy.returns('token1');
      const invalidUser = true;

      serviceApp.__set__('verifyPassword', () => false);

      const data = await login(spy(), spy());

      assert.deepEqual({ ...data }, { invalidUser });
    });

    it('Should return error when verifyPassword fail', async () => {
      const errSpy = stub();
      errSpy.returns(new WrapperError('WC9', 500, 'Error al verificar password'));

      serviceApp.__set__('verifyPassword', () => Promise.reject(new Error()));
      serviceApp.__set__('getError', errSpy);

      try {
        await login(spy(), spy());
      } catch (e) {
        assert.equal(e.code, 'WC9');
        assert.equal(e.status, 500);
        assert.equal(e.message, 'Error al verificar password');
      }
    });

    it('Should return error when generateToken fail', async () => {
      const errSpy = stub();
      errSpy.returns(new WrapperError('WC10', 500, 'Error al generar el token'));

      serviceApp.__set__('verifyPassword', () => true);
      serviceApp.__set__('generateToken', () => Promise.reject(new Error()));
      serviceApp.__set__('getError', errSpy);

      try {
        await login(spy(), spy());
      } catch (e) {
        assert.equal(e.code, 'WC10');
        assert.equal(e.status, 500);
        assert.equal(e.message, 'Error al generar el token');
      }
    });
  });

  describe('Token validation ', () => {
    beforeEach(() => {
      isValidToken = serviceApp.__get__('isValidToken');
    });

    it('Should return payload when verify token is success', async () => {
      const payload = { username: 'emeneses', id: '123' };

      serviceApp.__set__('verifyToken', () => (payload));

      const payloadResponse = await isValidToken('token1');

      assert.deepEqual(payloadResponse, payload);
    });

    it('Should return error when verify token is not valid', async () => {
      const errorData = ['WC012', 500, 'Token invalido'];
      serviceApp.__set__('verifyToken', () => (Promise.reject(new Error())));
      serviceApp.__set__('getError', () => (new WrapperError(...errorData)));

      try {
        await isValidToken('token1');
      } catch (e) {
        assert.equal(e.code, errorData[0]);
        assert.equal(e.status, errorData[1]);
        assert.equal(e.message, errorData[2]);
      }
    });
  });

  describe('Validate basic authorization ', () => {
    beforeEach(() => {
      isValidBasicAuthorization = serviceApp.__get__('isValidBasicAuthorization');
    });

    it('Should return success validation when app is sigin and the client secret '
      + 'and client id are vvalid ', async () => {
      const config = {
        secretsAPP: {
          app: {
            clientSecret: '1',
            clientId: '2',
          },
        },
      };

      serviceApp.__set__('config', config);

      const isValid = isValidBasicAuthorization('1', '2', 'app');

      assert.isTrue(isValid);
    });

    it('Should return invalid validation when app do not exist ', async () => {
      const config = {
        secretsAPP: {
          app1: {
            clientSecret: '1',
            clientId: '2',
          },
        },
      };

      serviceApp.__set__('config', config);

      const isValid = isValidBasicAuthorization('1', '2', 'app');

      assert.isTrue(!isValid);
    });

    it('Should return invalid validation when client secret is not valid', async () => {
      const config = {
        secretsAPP: {
          app: {
            clientSecret: '1',
            clientId: '2',
          },
        },
      };

      serviceApp.__set__('config', config);

      const isValid = isValidBasicAuthorization('3', '2', 'app');

      assert.isFalse(isValid);
    });

    it('Should return invalid validation when client id is not valid', async () => {
      const config = {
        secretsAPP: {
          app: {
            clientSecret: '1',
            clientId: '2',
          },
        },
      };

      serviceApp.__set__('config', config);

      const isValid = isValidBasicAuthorization('1', '4', 'app');

      assert.isFalse(isValid);
    });
  });
});
