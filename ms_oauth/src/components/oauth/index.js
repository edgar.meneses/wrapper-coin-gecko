const express = require('express');

const router = express.Router();
const { login, verifyToken } = require('./controller');
const Login = require('./schema/Login');
const sanitization = require('../../middleware/sanitization');

router.post('/login', Login, sanitization, login);
router.get('/validate', verifyToken);

module.exports = router;
