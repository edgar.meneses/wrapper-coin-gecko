const { encode, decode } = require('js-base64');
const { getError } = require('../../commons/errors/handlerError');
const {
  login: loginService,
  findUser,
  isValidToken,
  isValidBasicAuthorization,
} = require('./service');

const login = async (req, res, next) => {
  try {
    const { body } = req;
    const userId = encode(body.username);
    const user = await findUser(userId);
    const { invalidUser, token } = await loginService(user.data.userInfo, body);

    if (invalidUser) {
      next(getError(null, 'userInvalid', { method: 'login' }));
    } else {
      res.status(200).json({
        data: {
          token,
        },
      });
    }
  } catch (e) {
    next(getError(e, 'verifyToken', { method: 'controller - login' }));
  }
};

const verifyToken = async (req, res, next) => {
  try {
    const authorization = req.get('Authorization');

    if (authorization.includes('Bearer')) {
      const token = authorization.replace('Bearer ', '');

      const payload = await isValidToken(token);

      res.status(200).json({ data: { payload } });
    } else if (authorization.includes('Basic')) {
      const token = authorization.replace('Basic ', '');
      const clientId = req.get('X-Authorization-Client-Id');
      const clientSecret = req.get('X-Authorization-Client-Secret');
      const application = decode(token);

      const isValid = isValidBasicAuthorization(clientSecret, clientId, application);

      if (isValid) {
        const payload = {
          authorization: true,
          application: token,
        };

        res.status(200).json({ data: { payload } });
      } else {
        next(getError(null, 'invalidToken', { method: 'invalid token', isValid }));
      }
    } else {
      next(getError(null, 'invalidToken', { method: 'invalid method' }));
    }
  } catch (e) {
    next(getError(null, 'invalidToken', { method: 'invalidToken' }));
  }
};

module.exports = {
  login,
  verifyToken,
};
