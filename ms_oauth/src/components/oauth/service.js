const { encode } = require('js-base64');
const { verifyPassword } = require('../../commons/encriptPassword');
const { generateToken, verifyToken } = require('../../commons/jwt');
const { httpGetRequest } = require('../../commons/request');
const config = require('../../startup/config');
const { getError } = require('../../commons/errors/handlerError');

const findUser = async (useId) => {
  try {
    const application = 'ms_oauth:WCG';
    const secrets = config.secretsAPP[application];

    const user = await httpGetRequest(`${config.userRequestURL}/users/${useId}`,
      'userInvalid', null, {
        Authorization: `Basic ${encode(application)}`,
        'X-Authorization-Client-Secret': secrets.clientSecret,
        'X-Authorization-Client-Id': secrets.clientId,
      });

    return user;
  } catch (e) {
    throw getError(e, 'userInvalid', { method: 'login' });
  }
};

const login = async (user, loginData) => {
  try {
    const verify = await verifyPassword(loginData.password, user.password);
    if (verify) {
      const token = await generateToken({
        id: user.id,
        currency: user.favoriteCurrency,
      });
      return { token };
    }
    return { invalidUser: true };
  } catch (e) {
    throw getError(e, 'verifyToken', { method: 'service - verify password' });
  }
};

const isValidToken = async (token) => {
  try {
    const payload = await verifyToken(token);
    return payload;
  } catch (e) {
    throw getError(e, '', { method: 'service - isValidToken' });
  }
};

const isValidBasicAuthorization = (clientSecret, clientId, application) => {
  const secrets = config.secretsAPP[application];

  return (secrets && clientSecret === secrets.clientSecret && clientId === secrets.clientId);
};

module.exports = {
  login,
  findUser,
  isValidToken,
  isValidBasicAuthorization,
};
