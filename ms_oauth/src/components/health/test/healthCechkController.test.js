const { assert } = require('chai');
const {
  spy,
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');

let sandbox;
let healthCheck;
let req;
let res;

describe('Health check controller test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
    req = spy();
    res = {
      status: stub(),
      json: stub(),
    };
    res.status.returns(res);
  });

  afterEach(() => {
    sandbox.restore();
  });
  describe('Controller responses', () => {
    beforeEach(() => {
      const healthCheckApp = rewire('../controller');
      healthCheck = healthCheckApp.__get__('healthCheck');
    });

    it('Should return success response when health check is called', () => {
      const expectResponse = {
        data: { message: 'Server running success' },
      };

      healthCheck(req, res);

      assert.equal(res.status.args[0][0], 200);
      assert.deepEqual(res.json.args[0][0], expectResponse);
    });
  });
});
