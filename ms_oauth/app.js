const server = require('./src/startup/server');
const config = require('./src/startup/config');

const startServer = (_server, _config) => {
  _server(_config);
};

startServer(server, config);
