const { assert } = require('chai');
const {
  spy,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');

let sandbox;
let startServer;
let server;

describe('Server test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
  });

  describe('Start server', () => {
    beforeEach(() => {
      server = rewire('../server.js');
      startServer = server.__get__('startServer');
      server.__set__('routes', spy());
      server.__set__('info', spy());
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('the server should be running success when config port exits and listen function is called with same port.', async () => {
      const listen = spy();
      server.__set__('app', { listen });
      const config = { port: 9093 };

      startServer(config);

      assert.equal(listen.args[0][0], config.port);
    });

    it('the server should be running success when config port exists and express.listen function is executed.', async () => {
      const config = { port: 9092 };

      const start = startServer(config);

      assert.equal(start._connectionKey, `6::::${config.port}`);
    });

    it('the server should be running success when config port do not exists and express.listen function is executed.', async () => {
      const config = {};

      const start = startServer(config);

      assert.equal(start._connectionKey, '6::::9091');
    });
  });
});
