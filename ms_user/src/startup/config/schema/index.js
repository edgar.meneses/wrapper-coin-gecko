const schema = {
  env: {
    doc: 'It is the application execution environment variable.',
    format: ['local', 'dev', 'qa', 'pdn'],
    env: 'ENVIRONMENT',
    default: 'local',
  },
  port: {
    doc: 'It is the port of execution the application',
    format: Number,
    env: 'PORT',
    default: 9091,
  },
  userTableName: {
    doc: 'It is name of dynamoDB table to save user data',
    format: String,
    default: '',
  },
  aws: {
    region: {
      doc: 'It is the aws region',
      format: String,
      default: 'local',
    },
    endpoint: {
      doc: 'It is host where the dynamodb server is hosted',
      format: 'String',
      default: '',
    },
    dynamoDB: {
      apiVersion: {
        doc: 'It is dynamodb version of API',
        format: String,
        default: '',
      },
    },
  },
  oauthRequestURL: {
    doc: 'It is the URL of ms_user',
    format: 'String',
    default: 'localhost:9092',
  },
};

module.exports = schema;
