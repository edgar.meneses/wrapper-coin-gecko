const helmet = require('helmet');
const noCache = require('nocache');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const healthCheck = require('../components/health');
const userRoutes = require('../components/user');
const { errorMiddleware, notFoundMiddleware } = require('../middleware/errorRequest');

const configRoutes = (app) => {
  app.use(helmet());
  app.use(helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
    },
  }));
  app.use(helmet.referrerPolicy());

  app.use(noCache());

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  app.use(morgan('[ :status ] - [ :method ] - [ :url ] - [ :response-time ] - [ :remote-addr ] - [ :date[iso] ]'));

  app.use('/wrapper-coin-gecko/v1/users', userRoutes);
  app.use('/wrapper-coin-gecko/v1', healthCheck);

  app.use(errorMiddleware);
  app.use(notFoundMiddleware);
};

module.exports = configRoutes;
