const { encode, decode } = require('js-base64');
const { getError } = require('../../commons/errors/handlerError');
const { save, findById } = require('./service');

const saveUser = async (req, res, next) => {
  const userData = req.body;
  const getUser = await findById(userData.username);

  if (getUser) {
    next(getError(null, 'userExists', { method: 'controller - saveUser' }));
  } else {
    try {
      const user = await save({
        ...userData,
        favoriteCurrency: userData.favoriteCurrency.toLowerCase(),
      });

      const id = encode(user.username);
      res.status(200).json({
        data: { userInfo: { id, ...user } },
      });
    } catch (e) {
      next(getError(e, 'saveUser', { method: 'controller - saveUser' }));
    }
  }
};

const findUserById = async (req, res, next) => {
  const userId = decode(req.params.userId);

  try {
    const user = await findById(userId);

    if (!user) {
      next(getError(null, 'userNotFound',
        { method: 'controller - findUserById' }));
    } else {
      res.status(200)
        .json({
          data: {
            userInfo: { id: req.params.userId, ...user },
          },
        });
    }
  } catch (e) {
    next(getError(e, 'findUser', { method: 'controller - findUserById' }));
  }
};

module.exports = {
  saveUser,
  findUserById,
};
