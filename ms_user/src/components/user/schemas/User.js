const { body } = require('express-validator');

const User = [
  body('firstname')
    .notEmpty()
    .withMessage('El campo firstname es obligatorio')
    .matches(/^[a-zA-Z ]*$/, 'g')
    .withMessage('El campo firstname no contiene un valor valido')
    .isLength({ min: 3, max: 100 })
    .withMessage('El campo firstname no contiene una longitud valida'),
  body('lastname')
    .notEmpty()
    .withMessage('El campo firstname es obligatorio')
    .matches(/^[a-zA-Z ]*$/, 'g')
    .withMessage('El campo firstname no contiene un valor valido')
    .isLength({ min: 3, max: 100 })
    .withMessage('El campo firstname no contiene una longitud valida'),
  body('username')
    .notEmpty()
    .withMessage('El campo username es obligatorio')
    .isAlphanumeric()
    .withMessage('El campo username no contiene un valor valido')
    .isLength({ min: 5, max: 50 })
    .withMessage('El campo username no contiene una longitud valida'),
  body('password')
    .notEmpty()
    .withMessage('El campo password es obligatorio')
    .isAlphanumeric()
    .withMessage('El campo password no contiene un valor valido')
    .isLength({ min: 8 })
    .withMessage('El campo passwod no contiene una longitud valida'),
  body('favoriteCurrency')
    .notEmpty()
    .withMessage('el campo favoriteCurrency es obligatorio')
    .custom((val) => val.toLowerCase() === 'eur'
      || val.toLowerCase() === 'usd' || val.toLowerCase() === 'ars')
    .withMessage('el campo favoriteCurrency no contiene un valor valido'),
];

module.exports = User;
