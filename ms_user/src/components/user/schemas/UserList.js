const { param } = require('express-validator');

const UserList = [
  param('userId')
    .notEmpty()
    .withMessage('El Id de usuario es requerido')
    .isBase64()
    .withMessage('El Id de usuario no contine un valo valido'),
];

module.exports = UserList;
