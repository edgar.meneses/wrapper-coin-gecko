const express = require('express');

const router = express.Router();
const { saveUser, findUserById } = require('./controller');
const authorization = require('../../middleware/authorization');
const User = require('./schemas/User');
const UserList = require('./schemas/UserList');
const sanitization = require('../../middleware/sanitization');

router.post('/sigin', User, sanitization, saveUser);
router.get('/:userId', UserList, sanitization, authorization, findUserById);

module.exports = router;
