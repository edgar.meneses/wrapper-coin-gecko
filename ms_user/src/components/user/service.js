const { saveItem, getItem } = require('../../commons/aws/dynamodb');
const config = require('../../startup/config');
const WrapperError = require('../../commons/errors/wrapperError');
const { getError } = require('../../commons/errors/handlerError');
const { hashPassword } = require('../../commons/encriptPassword');

const save = async (user) => {
  try {
    const newUser = await saveItem({
      ...user,
      password: await hashPassword(user.password),
    }, config.userTableName);

    return (newUser.Attributes)
      ? { ...newUser.Attributes, password: undefined }
      : { ...user, password: undefined };
  } catch (e) {
    throw getError(e, 'saveUser', { method: 'service - saveUser' });
  }
};

const findById = async (key) => {
  try {
    const user = await getItem({ username: key }, config.userTableName);
    return user;
  } catch (e) {
    throw getError(e, 'findUser', { method: 'service - findById' });
  }
};

module.exports = {
  save,
  findById,
};
