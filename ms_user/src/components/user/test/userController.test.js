const { assert } = require('chai');
const {
  spy,
  stub,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../../commons/errors/wrapperError');

let sandbox;
let userApp;
let saveUser;
let res;
let next;
let errorSpy;
let findUserById;

describe('User controller test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
    res = {
      status: stub(),
      json: stub(),
    };
    next = spy();
    errorSpy = stub();

    res.status.returns(res);

    userApp = rewire('../controller');
  });

  afterEach(() => {
    sandbox.restore();
  });
  describe('User save', () => {
    beforeEach(() => {
      saveUser = userApp.__get__('saveUser');
    });

    it('Should return user information when sreave function is executed success', async () => {
      const newUser = {
        body: {
          firstName: 'Edgar',
          lastName: 'Meneses',
          username: 'emeneses',
          password: '123',
          favoriteCurrency: 'USD',
        },
      };
      const id = 'ZW1lbmVzZXM=';
      const save = stub();
      save.returns(newUser.body);

      userApp.__set__('save', save);
      userApp.__set__('findById', spy());

      await saveUser(newUser, res);

      assert.deepEqual(res.json.args[0][0], {
        data: { userInfo: { id, ...newUser.body } },
      });
    });

    it('Should return error when user save controller fail', async () => {
      const newUser = {
        body: { firstName: 'Antonio' },
      };
      const save = stub();
      save.returns(Promise.reject());

      const errorInfo = ['WC001', 500, 'Error al guardar el usuario'];

      errorSpy.returns(new WrapperError(...errorInfo));

      userApp.__set__('getError', errorSpy);
      userApp.__set__('findById', spy());
      userApp.__set__('save', save);

      await saveUser(newUser, res, next);

      assert.equal(next.args[0][0].code, errorInfo[0]);
      assert.equal(next.args[0][0].status, errorInfo[1]);
      assert.equal(next.args[0][0].message, errorInfo[2]);
    });

    it('Should return error when user exits', async () => {
      const newUser = {
        body: { firstName: 'Antonio' },
      };
      const findById = stub();
      findById.returns(newUser.body);

      const errorInfo = ['WC010', 401, 'No es posile almacenar el usuario'];
      errorSpy.returns(new WrapperError(...errorInfo));

      userApp.__set__('getError', errorSpy);
      userApp.__set__('findById', findById);

      await saveUser(newUser, res, next);

      assert.equal(next.args[0][0].message, errorInfo[2]);
    });
  });

  describe('Ge user information by id', () => {
    beforeEach(() => {
      findUserById = userApp.__get__('findUserById');
    });

    it('Should return user when function findById run success', async () => {
      const req = {
        params: { userId: 'ZW1lbmVzZXM5Mg==' },
      };

      const userData = {
        username: 'emeneses92',
        firstName: 'Edgar',
        lattName: 'Meneses',
      };

      const findByIdSpy = stub();
      findByIdSpy.returns(userData);

      userApp.__set__('findById', findByIdSpy);

      await findUserById(req, res, next);

      assert.deepEqual(res.json.args[0][0], {
        data: {
          userInfo: { id: req.params.userId, ...userData },
        },
      });
    });

    it('Should return error when function findById fail', async () => {
      const req = {
        params: { userId: 'ZW1lbmVzZXM5Mg==' },
      };

      const findByIdSpy = stub();
      findByIdSpy.returns(Promise.reject());

      const wrapperError = ['WCG011', 500, 'Error al consultar el usuario'];
      const errSpy = stub();
      errSpy.returns(wrapperError);

      userApp.__set__('findById', findByIdSpy);
      userApp.__set__('getError', errSpy);

      try {
        await findUserById(req, res, next);
      } catch (e) {
        assert.equal(e.code, wrapperError[0]);
        assert.equal(e.status, wrapperError[1]);
        assert.equal(e.message, wrapperError[2]);
      }
    });

    it('Should return error when user is not found', async () => {
      const req = {
        params: {
          userId: 'YKL5=',
        },
      };
      const errorData = ['WC78', 404, 'User not found'];

      userApp.__set__('findById', () => undefined);
      userApp.__set__('getErro', () => (new WrapperError(...errorData)));

      try {
        await findUserById(req, res, next);
      } catch (e) {
        assert.equal(next.args[0][0].code, errorData[0]);
        assert.equal(next.args[0][0].status, errorData[1]);
        assert.equal(next.args[0][0].message, errorData[2]);
      }
    });
  });
});
