const { assert } = require('chai');
const {
  stub,
  spy,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../../commons/errors/wrapperError');

let sandbox;
let save;
let serviceApp;
let userData;
let findById;

describe('Save user test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();

    userData = {
      username: 'emeneses23',
      firstName: 'Edgar A',
      lastName: 'Meneses ',
      favoriteCurrency: 'USD',
    };

    serviceApp = rewire('../service');
    serviceApp.__set__('hashPassword', spy());
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Save new user', () => {
    beforeEach(() => {
      save = serviceApp.__get__('save');
    });

    it('Should save user when function save item run success', async () => {
      const saveItemSpy = stub();
      saveItemSpy.returns(Promise.resolve(userData));
      serviceApp.__set__('saveItem', saveItemSpy);

      const user = await save(userData);

      assert.deepEqual(user, { ...userData, password: undefined });
    });

    it('Should return user data in Attributes key when save item run success', async () => {
      const saveItemSpy = stub();
      saveItemSpy.returns(Promise.resolve({ Attributes: userData }));
      serviceApp.__set__('saveItem', saveItemSpy);

      const user = await save(userData);

      assert.deepEqual(user, { ...userData, password: undefined });
    });

    it('Should return error when save item fail', async () => {
      const errorSave = ['WC008', 500, 'Error al guardar el usuario'];
      const errSpy = stub();
      errSpy.returns(new WrapperError(...errorSave));

      serviceApp.__set__('getError', errSpy);
      serviceApp.__set__('saveItem', Promise.reject());

      try {
        await save(userData);
      } catch (e) {
        assert.equal(e.code, errorSave[0]);
        assert.equal(e.status, errorSave[1]);
        assert.equal(e.message, errorSave[2]);
      }
    });
  });

  describe('Find user by id', () => {
    beforeEach(() => {
      findById = serviceApp.__get__('findById');
    });

    it('Should return user data when dynamodb.getItem run success', async () => {
      const getUserSpy = stub();
      getUserSpy.returns(Promise.resolve({ ...userData }));
      serviceApp.__set__('getItem', getUserSpy);

      const user = await findById(userData);

      assert.deepEqual(user, { ...userData });
    });

    it('Should return user data when dynamodb.getItem fail', async () => {
      const errorSave = ['WC009', 500, 'Error al consultar el usuario'];
      const errSpy = stub();
      errSpy.returns(new WrapperError(...errorSave));

      serviceApp.__set__('getError', errSpy);
      serviceApp.__set__('getItem', Promise.reject());

      try {
        await findById(userData);
      } catch (e) {
        assert.equal(e.code, errorSave[0]);
        assert.equal(e.status, errorSave[1]);
        assert.equal(e.message, errorSave[2]);
      }
    });
  });
});
