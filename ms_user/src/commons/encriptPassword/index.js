const bcrypt = require('bcrypt');
const { getError } = require('../errors/handlerError');
const config = require('../../startup/config');

const hashPassword = async (password) => {
  try {
    const result = await bcrypt.hash(password, config.roundSalt);
    return result;
  } catch (e) {
    throw getError(e, 'saveUser', { method: 'encode password' });
  }
};

module.exports = {
  hashPassword,
};
