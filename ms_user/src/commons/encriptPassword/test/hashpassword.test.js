const { assert } = require('chai');
const {
  stub,
  spy,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../errors/wrapperError');

let sandbox;
let hashPassword;
let encriptApp;

describe('Encript password test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
    const hash = stub();
    hash.returns('AAABB');

    encriptApp = rewire('../index.js');
    encriptApp.__set__('bcrypt', {
      hash,
    });
    encriptApp.__set__('config', { roundSalt: spy() });

    hashPassword = encriptApp.__get__('hashPassword');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('hash password', () => {
    it('Should return password hash', async () => {
      const hashResult = await hashPassword('99999999');

      assert.equal(hashResult, 'AAABB');
    });

    it('Should return error when generate token fail', async () => {
      const errorData = ['WC09', 500, 'Error al generar el token'];
      const bycryptSpy = stub();
      bycryptSpy.returns({ hash: () => Promise.reject() });

      encriptApp.__set__('bcrypt', bycryptSpy);
      encriptApp.__set__('getError', () => (new WrapperError(...errorData)));

      try {
        await hashPassword('mm');
      } catch (e) {
        assert.equal(e.code, errorData[0]);
        assert.equal(e.status, errorData[1]);
        assert.equal(e.message, errorData[2]);
      }
    });
  });
});
