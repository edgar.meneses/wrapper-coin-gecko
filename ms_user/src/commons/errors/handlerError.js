const uuid = require('uuid/v4');
const { errors } = require('./errorList');
const WrapperError = require('./wrapperError');

const handlerError = (errorList) => (errorKey) => (errorList[errorKey]
  ? [...errorList[errorKey]]
  : ['WCG-000', 500, 'Error undefined']);

const getError = (error, errorName, detail) => {
  if (error instanceof WrapperError) {
    return error;
  }

  const handler = handlerError(errors);
  return new WrapperError(...handler(errorName), error, detail);
};

const getInfoErrorFromStatus = (statusCode) => {
  const data = [
    { statusCode: 400, title: 'Bad Request', url: 'https://tools.ietf.org/html/rfc7231#section-6.5.1' },
    { statusCode: 401, title: 'Unauthorized', url: 'https://tools.ietf.org/html/rfc7231#section-3.1' },
    { statusCode: 403, title: 'Forbidden', url: 'https://tools.ietf.org/html/rfc7231#section-6.5.3' },
    { statusCode: 404, title: 'Not Found', url: 'https://tools.ietf.org/html/rfc7231#section-6.5.4' },
    { statusCode: 500, title: 'Internal Server Error', url: 'https://tools.ietf.org/html/rfc7231#section-6.6.1' },
    { statusCode: 503, title: 'Service Unavailable', url: 'https://tools.ietf.org/html/rfc7231#section-6.6.4' },
  ];

  return data.find((x) => x.statusCode === statusCode);
};

const getRequestError = (error) => {
  const statusCode = error.status || 500;
  const infoError = getInfoErrorFromStatus(statusCode);

  return {
    error: {
      id: uuid(),
      href: infoError.url,
      status: statusCode,
      code: error.code || 'NOT DEFINED',
      title: infoError.title,
      detail: error.message || 'NOT DEFINED',
      trace: error.detail,
    },
  };
};

module.exports = {
  getError,
  getRequestError,
};
