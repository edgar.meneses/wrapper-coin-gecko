const { assert } = require('chai');
const {
  createSandbox,
  stub,
} = require('sinon');
const rewire = require('rewire');

let sandbox;
let dynamodbAPP;
let saveItem;
let getItem;
let dynamoDBSpy;
let item;

describe('Dynamodb options', () => {
  beforeEach(() => {
    item = {
      username: 'emeneses',
      firstName: 'Edgar',
      lastName: 'Meneses',
    };

    sandbox = createSandbox();
    dynamodbAPP = rewire('../index.js');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Dynamodb put items', () => {
    beforeEach(() => {
      saveItem = dynamodbAPP.__get__('saveItem');
    });

    it('Should return user save when dynamodb.put run success', async () => {
      dynamoDBSpy = {
        put: (params, callback) => {
          callback(null, { ...item });
        },
      };

      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);

      const newUser = await saveItem(item, 'users');

      assert.deepEqual(newUser, item);
    });

    it('Should return error when dynamodb.put fail', async () => {
      dynamoDBSpy = {
        put: (prams, callback) => {
          callback(new Error(''));
        },
      };
      const errorSpy = stub();
      errorSpy.returns(new Error('Error al guardar la información'));

      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);
      dynamodbAPP.__set__('getError', errorSpy);
      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);
      dynamodbAPP.__set__('getError', errorSpy);

      try {
        await saveItem(item, 'users');
      } catch (e) {
        assert.equal(e.message, 'Error al guardar la información');
      }
    });
  });

  describe('Dynamodb get item', () => {
    beforeEach(() => {
      getItem = dynamodbAPP.__get__('getItem');
    });

    it('Should return item when dynamodb.get run success', async () => {
      dynamoDBSpy = {
        get: (prams, callback) => {
          callback(null, { Item: { ...item } });
        },
      };
      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);

      const itemData = await getItem({ username: 'emeneses' }, 'user');

      assert.deepEqual(itemData, item);
    });

    it('Should return error when dynamodb.get fail', async () => {
      dynamoDBSpy = {
        get: (prams, callback) => {
          callback(new Error());
        },
      };
      const errorSpy = stub();
      errorSpy.returns(new Error('Error al consultar el item'));

      dynamodbAPP.__set__('getError', errorSpy);
      dynamodbAPP.__set__('dynamoDB', dynamoDBSpy);

      try {
        await getItem({ username: 'emeneses' }, 'user');
      } catch (e) {
        assert.equal(e.message, 'Error al consultar el item');
      }
    });
  });
});
