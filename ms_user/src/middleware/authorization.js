const { httpGetRequest } = require('../commons/request');
const config = require('../startup/config');
const { getError } = require('../commons/errors/handlerError');

const authorization = async (req, res, next) => {
  const token = req.get('Authorization') || '';
  const clientSecret = req.get('X-Authorization-Client-Secret') || '';
  const clientId = req.get('X-Authorization-Client-Id') || '';

  try {
    const response = await httpGetRequest(`${config.oauthURL}/oauth/validate`,
      '', null, {
        Authorization: token,
        'X-Authorization-Client-Secret': clientSecret,
        'X-Authorization-Client-Id': clientId,
      });

    req.user = response;
    next();
  } catch (e) {
    next(getError(null, 'invalidToken', { method: 'authorization' }));
  }
};

module.exports = authorization;
