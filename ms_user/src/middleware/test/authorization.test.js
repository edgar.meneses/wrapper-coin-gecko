const { assert } = require('chai');
const {
  stub,
  spy,
  createSandbox,
} = require('sinon');
const rewire = require('rewire');
const WrapperError = require('../../commons/errors/wrapperError');

let sandbox;
let res;
let req;
let next;
let authorizationApp;
let authorization;

describe('Authorization middleware test suite', () => {
  beforeEach(() => {
    sandbox = createSandbox();
    res = {
      status: stub(),
      json: stub(),
    };
    next = spy();

    res.status.returns(res);

    authorizationApp = rewire('../authorization');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('authorization ', () => {
    beforeEach(() => {
      authorization = authorizationApp.__get__('authorization');
    });

    it('Should return authorization success when validate token is valid', async () => {
      const payload = { username: 'emeneses', id: '123' };
      req = {
        get: spy(),
      };

      authorizationApp.__set__('httpGetRequest', () => payload);

      await authorization(req, res, next);

      assert.deepEqual(req.user, payload);
    });

    it('Should return authorization success when Authorization header is valid and'
      + 'clientId and clientSecret are undefined', async () => {
      const payload = { username: 'emeneses', id: '123' };
      req = {
        get: (param) => ((param === 'Authorization') ? 'data' : undefined),
      };

      authorizationApp.__set__('httpGetRequest', () => payload);

      await authorization(req, res, next);

      assert.deepEqual(req.user, payload);
    });

    it('Should return authorization success when ClientSecret and ClientId are valid and'
      + 'Authorization is undefined', async () => {
      const payload = { username: 'emeneses', id: '123' };
      req = {
        get: (param) => ((param !== 'Authorization') ? 'data' : undefined),
      };

      authorizationApp.__set__('httpGetRequest', () => payload);

      await authorization(req, res, next);

      assert.deepEqual(req.user, payload);
    });

    it('Should return error success when token is invalid', async () => {
      const errorData = ['WC67', 401, 'Invalid'];
      req = {
        get: spy(),
      };

      authorizationApp.__set__('httpGetRequest', () => Promise.reject());
      authorizationApp.__set__('getError', () => (new WrapperError(...errorData)));

      try {
        await authorization(req, res, next);
      } catch (e) {
        assert.equal(next.args[0][0].code, errorData[0]);
        assert.equal(next.args[0][0].status, errorData[1]);
        assert.equal(next.args[0][0].message, errorData[2]);
      }
    });
  });
});
